const { createApolloFetch } = require('apollo-fetch')

var apolloFetch = null

// set access token
exports.setToken = (token, uri = 'https://api.lotto2day.co/graphql') => {
  apolloFetch = createApolloFetch({ uri })
  apolloFetch.use(({ request, options }, next) => {
    if (!options.headers) {
      options.headers = {} // Create the headers object if needed.
    }
    options.headers['authorization'] = `Bearer ${token}`
    next()
  })
}

// login with username and password
exports.login = ({ username, password }) => {
  return apolloFetch({
    query: `query login ($username: String!, $password: String!) {
      login(username: $username, password: $password) {
        userid
        username
        email
        permission
        token
      }
    }`,
    variables: {
      username,
      password,
    },
  })
}

// create/update prizes
// access token with write permission is required
exports.update = ({
  date,
  first,
  head_3,
  tail_3,
  tail_2,
  first_nb,
  second,
  third,
  forth,
  fifth,
  pdf,
}) => {
  return apolloFetch({
    query: `mutation updatePrize ($input: PrizeInput!) {
      updatePrize(input: $input) {
        date,
        first,
        head_3,
        tail_3,
        tail_2,
        first_nb,
        second,
        third,
        forth,
        fifth,
        pdf
      }
    }`,
    variables: {
      input: {
        date,
        first,
        head_3,
        tail_3,
        tail_2,
        first_nb,
        second,
        third,
        forth,
        fifth,
        pdf,
      },
    },
  })
}

// submit custom query
exports.query = (query, variables = {}) => {
  return apolloFetch({
    query: `${query}`,
    variables: variables,
  })
}

// return dates that lotto wins categorized by prizes
exports.wins = (lotto) => {
  return apolloFetch({
    query: `query wins($lotto: String!){
        wins(lotto: $lotto) {
          first,
          head_3,
          tail_3,
          tail_2,
          first_nb,
          second,
          third,
          forth,
          fifth
        }
      }`,
    variables: {
      lotto,
    },
  })
}

// return prizes on date
exports.prize = (
  date,
  fields = [
    'first',
    'head_3',
    'tail_3',
    'tail_2',
    'first_nb',
    'second',
    'third',
    'forth',
    'fifth',
  ]
) => {
  return apolloFetch({
    query: `query prizes($date: String!){
        prize (date: $date) {
          ${fields.join(', ')}
        }
      }`,
    variables: {
      date,
    },
  })
}

// DEPRECATED
// return start and end dates of last 100 data
exports.period = () => {
  return apolloFetch({
    query: `query period {
        period {
          start,
          end
        }
      }`,
  })
}

// return the dates of the last n prize data
exports.dates = (n, fields = ['n', 'start', 'end', 'dates']) => {
  return apolloFetch({
    query: `query dates ($n: Int){
        dates (n: $n) {
          ${fields.join(', ')}
        }
      }`,
    variables: {
      n,
    },
  })
}
