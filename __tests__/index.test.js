const { setToken, wins, prize, query, dates } = require('../src/index')

setToken(
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyaWQiOiJhZmVkMjQ3Ni0xNzM2LTQ3ZGYtYWE1NS1hZTBkYjM0OTM4ZGMiLCJ1c2VybmFtZSI6InB1YmxpYyIsImVtYWlsIjoicHVibGljQHBlbmdicm9zLmNvbSIsInBlcm1pc3Npb24iOlsicmVhZCJdLCJpYXQiOjE1MjIwNTIwNDB9.KOtK1swODnDfWWJpUZmrtCmZH8rSdL3J1tibUHzFcPs'
)

test('wins', async () => {
  const result = await wins('888598')
  expect(result.data.wins.tail_3[0]).toEqual('2015-08-01')
})

test('prize with no fields', async () => {
  const result = await prize('2019-07-01')
  expect(result.data.prize.first[0]).toEqual('943647')
})

test('prize with fields', async () => {
  const result = await prize('2019-07-01', ['first'])
  expect(result.data.prize.first[0]).toEqual('943647')
})

test('query', async () => {
  const q = `query wins($lotto: String!){
    wins(lotto: $lotto) {
      first,
      head_3,
      tail_3,
      tail_2,
      first_nb,
      second,
      third,
      forth,
      fifth
    }
  }`

  const result = await query(q, { lotto: '888598' })
  expect(result.data.wins.tail_3[0]).toEqual('2015-08-01')
})

test('dates', async () => {
  const result = await dates(10000)
  expect(result.data.dates.start).toEqual('2010-12-30')
})

test('dates with fields', async () => {
  const result = await dates(10000, ['start'])
  expect(result.data.dates.start).toEqual('2010-12-30')
})
