'use strict';

var _require = require('apollo-fetch'),
    createApolloFetch = _require.createApolloFetch;

var apolloFetch = null;

// set access token
exports.setToken = function (token) {
  var uri = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'https://api.lotto2day.co/graphql';

  apolloFetch = createApolloFetch({ uri: uri });
  apolloFetch.use(function (_ref, next) {
    var request = _ref.request,
        options = _ref.options;

    if (!options.headers) {
      options.headers = {}; // Create the headers object if needed.
    }
    options.headers['authorization'] = 'Bearer ' + token;
    next();
  });
};

// login with username and password
exports.login = function (_ref2) {
  var username = _ref2.username,
      password = _ref2.password;

  return apolloFetch({
    query: 'query login ($username: String!, $password: String!) {\n      login(username: $username, password: $password) {\n        userid\n        username\n        email\n        permission\n        token\n      }\n    }',
    variables: {
      username: username,
      password: password
    }
  });
};

// create or update prizes collection
exports.update = function (_ref3) {
  var date = _ref3.date,
      first = _ref3.first,
      head_3 = _ref3.head_3,
      tail_3 = _ref3.tail_3,
      tail_2 = _ref3.tail_2,
      first_nb = _ref3.first_nb,
      second = _ref3.second,
      third = _ref3.third,
      forth = _ref3.forth,
      fifth = _ref3.fifth,
      pdf = _ref3.pdf;

  return apolloFetch({
    query: 'mutation updatePrize ($input: PrizeInput!) {\n      updatePrize(input: $input) {\n        date,\n        first,\n        head_3,\n        tail_3,\n        tail_2,\n        first_nb,\n        second,\n        third,\n        forth,\n        fifth,\n        pdf\n      }\n    }',
    variables: {
      input: {
        date: date,
        first: first,
        head_3: head_3,
        tail_3: tail_3,
        tail_2: tail_2,
        first_nb: first_nb,
        second: second,
        third: third,
        forth: forth,
        fifth: fifth,
        pdf: pdf
      }
    }
  });
};

// fetch dates that lotto wins
exports.wins = function (lotto) {
  return apolloFetch({
    query: 'query wins($lotto: String!){\n        wins(lotto: $lotto) {\n          first,\n          head_3,\n          tail_3,\n          tail_2,\n          first_nb,\n          second,\n          third,\n          forth,\n          fifth\n        }\n      }',
    variables: {
      lotto: lotto
    }
  });
};

// fetch prizes on date
exports.prize = function (date) {
  var fields = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ['first', 'head_3', 'tail_3', 'tail_2', 'first_nb', 'second', 'third', 'forth', 'fifth'];

  return apolloFetch({
    query: 'query prizes($date: String!){\n        prize (date: $date) {\n          ' + fields.join(', ') + '\n        }\n      }',
    variables: {
      date: date
    }
  });
};

// fetch dates that lotto wins
exports.period = function () {
  return apolloFetch({
    query: 'query period {\n        period {\n          start,\n          end\n        }\n      }'
  });
};